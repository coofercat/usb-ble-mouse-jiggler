#include "EspUsbHost.h"

#include "BleMouse.h"

#include <RingBuf.h>

#include <esp_task_wdt.h>
#define WDT_TIMEOUT   2

// How long between jiggles?
#define INACTIVITY_TIMEOUT 270000
// How long until we give up jiggling?
#define JIGGLE_TIME 1800000

// How long is a jiggle?
#define JIGGLE_WIDTH 20

#define LED_MAX_BRIGHTNESS 64
#define LED_DIM_BRIGHTNESS 16

#define DEBUG_ON 1
#define DEBUG_OFF 0
byte debugMode = DEBUG_OFF;

#define DEBUG_OUTPUT(...) debugMode == DEBUG_ON ? Serial.println(__VA_ARGS__) : NULL
#define DEBUG_OUTPUTF(...) debugMode == DEBUG_ON ? Serial.printf(__VA_ARGS__) : NULL

#define EVENT_TYPE_BUTTON_PRESS 1
#define EVENT_TYPE_BUTTON_RELEASE 2
#define EVENT_TYPE_MOVE 3

typedef struct mouse_event {
  unsigned char type;
  unsigned char button;
  signed char x;
  signed char y;
  signed char wheel;
 };

RingBuf<mouse_event, 100> mouse_events_buffer;

BleMouse bleMouse("Hackaroony Mouse", "CorpSmash Industries");
unsigned long last_movement = millis();
unsigned long last_synthetic_movement = millis();

struct statusStruct {
  unsigned char inactive;
  unsigned char connected;
  unsigned char idle;
} status = { 0, 0, 0};

class MyEspUsbHost : public EspUsbHost {
  void onMouseButtons(hid_mouse_report_t report, uint8_t last_buttons) {
    DEBUG_OUTPUTF("last_buttons=0x%02x(%c%c%c%c%c), buttons=0x%02x(%c%c%c%c%c), x=%d, y=%d, wheel=%d\n",
                  last_buttons,
                  (last_buttons & MOUSE_BUTTON_LEFT) ? 'L' : ' ',
                  (last_buttons & MOUSE_BUTTON_RIGHT) ? 'R' : ' ',
                  (last_buttons & MOUSE_BUTTON_MIDDLE) ? 'M' : ' ',
                  (last_buttons & MOUSE_BUTTON_BACKWARD) ? 'B' : ' ',
                  (last_buttons & MOUSE_BUTTON_FORWARD) ? 'F' : ' ',
                  report.buttons,
                  (report.buttons & MOUSE_BUTTON_LEFT) ? 'L' : ' ',
                  (report.buttons & MOUSE_BUTTON_RIGHT) ? 'R' : ' ',
                  (report.buttons & MOUSE_BUTTON_MIDDLE) ? 'M' : ' ',
                  (report.buttons & MOUSE_BUTTON_BACKWARD) ? 'B' : ' ',
                  (report.buttons & MOUSE_BUTTON_FORWARD) ? 'F' : ' ',
                  report.x,
                  report.y,
                  report.wheel);

    // LEFT
    if (!(last_buttons & MOUSE_BUTTON_LEFT) && (report.buttons & MOUSE_BUTTON_LEFT)) {
      DEBUG_OUTPUT("Mouse LEFT Click");
      mouse_event e = {EVENT_TYPE_BUTTON_PRESS, MOUSE_LEFT, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.press(MOUSE_LEFT);
      last_movement = millis();
    }
    if ((last_buttons & MOUSE_BUTTON_LEFT) && !(report.buttons & MOUSE_BUTTON_LEFT)) {
      DEBUG_OUTPUT("Mouse LEFT Release");
      mouse_event e = {EVENT_TYPE_BUTTON_RELEASE, MOUSE_LEFT, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.release(MOUSE_LEFT);
      last_movement = millis();
    }

    // RIGHT
    if (!(last_buttons & MOUSE_BUTTON_RIGHT) && (report.buttons & MOUSE_BUTTON_RIGHT)) {
      DEBUG_OUTPUT("Mouse RIGHT Click");
      mouse_event e = {EVENT_TYPE_BUTTON_PRESS, MOUSE_RIGHT, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.press(MOUSE_RIGHT);
      last_movement = millis();
    }
    if ((last_buttons & MOUSE_BUTTON_RIGHT) && !(report.buttons & MOUSE_BUTTON_RIGHT)) {
      DEBUG_OUTPUT("Mouse RIGHT Release");
      mouse_event e = {EVENT_TYPE_BUTTON_RELEASE, MOUSE_RIGHT, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.release(MOUSE_RIGHT);
      last_movement = millis();
    }

    // MIDDLE
    if (!(last_buttons & MOUSE_BUTTON_MIDDLE) && (report.buttons & MOUSE_BUTTON_MIDDLE)) {
      DEBUG_OUTPUT("Mouse MIDDLE Click");
      mouse_event e = {EVENT_TYPE_BUTTON_PRESS, MOUSE_MIDDLE, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.press(MOUSE_MIDDLE);
      last_movement = millis();
    }
    if ((last_buttons & MOUSE_BUTTON_MIDDLE) && !(report.buttons & MOUSE_BUTTON_MIDDLE)) {
      DEBUG_OUTPUT("Mouse MIDDLE Release");
      mouse_event e = {EVENT_TYPE_BUTTON_RELEASE, MOUSE_MIDDLE, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.release(MOUSE_MIDDLE);
      last_movement = millis();
    }

    // BACKWARD
    if (!(last_buttons & MOUSE_BUTTON_BACKWARD) && (report.buttons & MOUSE_BUTTON_BACKWARD)) {
      DEBUG_OUTPUT("Mouse BACKWARD Click");
      mouse_event e = {EVENT_TYPE_BUTTON_PRESS, MOUSE_BACK, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.press(MOUSE_BACK);
      last_movement = millis();
    }
    if ((last_buttons & MOUSE_BUTTON_BACKWARD) && !(report.buttons & MOUSE_BUTTON_BACKWARD)) {
      DEBUG_OUTPUT("Mouse BACKWARD Release");
      mouse_event e = {EVENT_TYPE_BUTTON_RELEASE, MOUSE_BACK, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.release(MOUSE_BACK);
      last_movement = millis();
    }

    // FORWARD
    if (!(last_buttons & MOUSE_BUTTON_FORWARD) && (report.buttons & MOUSE_BUTTON_FORWARD)) {
      DEBUG_OUTPUT("Mouse FORWARD Click");
      mouse_event e = {EVENT_TYPE_BUTTON_PRESS, MOUSE_FORWARD, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.press(MOUSE_FORWARD);
      last_movement = millis();
    }
    if ((last_buttons & MOUSE_BUTTON_FORWARD) && !(report.buttons & MOUSE_BUTTON_FORWARD)) {
      DEBUG_OUTPUT("Mouse FORWARD Release");
      mouse_event e = {EVENT_TYPE_BUTTON_RELEASE, MOUSE_FORWARD, 0, 0, 0 };
      mouse_events_buffer.push(e);
      //bleMouse.release(MOUSE_FORWARD);
      last_movement = millis();
    }
  };

  void onMouseMove(hid_mouse_report_t report) {
    DEBUG_OUTPUTF("buttons=0x%02x(%c%c%c%c%c), x=%d, y=%d, wheel=%d\n",
                  report.buttons,
                  (report.buttons & MOUSE_BUTTON_LEFT) ? 'L' : ' ',
                  (report.buttons & MOUSE_BUTTON_RIGHT) ? 'R' : ' ',
                  (report.buttons & MOUSE_BUTTON_MIDDLE) ? 'M' : ' ',
                  (report.buttons & MOUSE_BUTTON_BACKWARD) ? 'B' : ' ',
                  (report.buttons & MOUSE_BUTTON_FORWARD) ? 'F' : ' ',
                  report.x,
                  report.y,
                  report.wheel);
    mouse_event e = {EVENT_TYPE_MOVE, 0, (signed char)report.x, (signed char)report.y, (signed char)report.wheel };
    mouse_events_buffer.push(e);
    //bleMouse.move((signed char)report.x, (signed char)report.y, (signed char)report.wheel);
    last_movement = millis();
  };
};

void set_led() {
  unsigned char brightness = LED_MAX_BRIGHTNESS;
  unsigned char colour = 0;
  if (status.connected) {
    colour = 3;
  } else {
    colour = 1;
  }
  if (status.inactive) {
    colour = 2;
  }
  if (status.idle) {
    brightness = LED_DIM_BRIGHTNESS;
  }
  switch(colour) {
    case 1:
      neopixelWrite(RGB_BUILTIN,brightness,0,0);
      break;
    case 2:
      neopixelWrite(RGB_BUILTIN,0,brightness,0);
      break;
    case 3:
      neopixelWrite(RGB_BUILTIN,0,0,brightness);
      break;
    default:
      neopixelWrite(RGB_BUILTIN,0,0,0);
      break;
  }
}

MyEspUsbHost usbHost;

void setup() {
  Serial.begin(115200);
  delay(500);
  esp_log_level_set("*", ESP_LOG_VERBOSE);
  Serial.println("");

  bleMouse.begin();

  usbHost.begin();

  set_led();

  last_movement = millis();

  int BootReason = esp_reset_reason();
  switch (BootReason) {
    case 1:
      Serial.println("Reboot was because of Power-On");
      break;
    case 6:
      Serial.println("Reboot was because of WDT");
      break;
    default:
      Serial.print("Reset/Boot Reason was: ");
      Serial.println( BootReason );
      break;
  }
  Serial.println("Configuring WatchDogTimeout WDT...");
  esp_task_wdt_init(WDT_TIMEOUT, true);             // enable panic so ESP32 restarts
  esp_task_wdt_add(NULL);                           // add current thread to WDT watch
}



unsigned long diff;
unsigned char in_jiggle = 0;
unsigned char count = 0;
void loop() {
  // Heat/energy reduction...
  // A bit of testing suggests this causes the code below it to run every ~15-20ms
  // which is hopefully enough resolution to do what we need without making this
  // a heavy spin-busy loop.
  if (bleMouse.isConnected()) {
    usbHost.task();
    count++;
    if (count < 10) {
      return;
    }
  } else {
    delay(100);
  }
  // Reset the watchdog so we don't self-reset
  esp_task_wdt_reset();

  // Send any outstanding mouse moves to the BLE
  mouse_event e;
  while (mouse_events_buffer.pop(e)) {
    switch (e.type) {
      case EVENT_TYPE_BUTTON_PRESS:
        bleMouse.press(e.button);
        break;
      case EVENT_TYPE_BUTTON_RELEASE:
        bleMouse.release(e.button);
        break;
      case EVENT_TYPE_MOVE:
        bleMouse.move(e.x, e.y, e.wheel);
        if(mouse_events_buffer.size() > 0) {
          delay(3);
        }
        break;
    }
  }

  count = 0;
  diff = millis() - last_movement;

  if (diff < JIGGLE_TIME) {
    status.idle = 0;
    if (diff > INACTIVITY_TIMEOUT) {
      // Only do anything if we've been idle long enough, but we haven't
      // been idle longer than the total jiggle time
      if (status.inactive == 0) {
        // Have just become inactive
        last_synthetic_movement = last_movement;
        DEBUG_OUTPUT("No mouse activity - moving to inactive");
        status.inactive = 1;
      }

      diff = millis() - last_synthetic_movement;
      
      if (in_jiggle == 0 && diff > INACTIVITY_TIMEOUT) {
        DEBUG_OUTPUT("Idle timeout - jiggling the mouse");
        bleMouse.move(-1, 0, 0);
        in_jiggle = 1;
        neopixelWrite(RGB_BUILTIN,LED_MAX_BRIGHTNESS,0,0);
      } else if (in_jiggle == 1 && diff > INACTIVITY_TIMEOUT + JIGGLE_WIDTH) {
        bleMouse.move(1, 0, 0);
        last_synthetic_movement = millis();
        in_jiggle = 0;
      }

    } else {
      if (status.inactive == 1) {
        DEBUG_OUTPUT("Activity detected - no longer inactive");
      }
      status.inactive = 0;
    }
  } else {
    if (status.idle == 0) {
      DEBUG_OUTPUT("Reached end of jiggle time - moving to idle");
    }
    status.idle = 1;
  }

  if (status.connected == 0) {
    if (bleMouse.isConnected()) {
      DEBUG_OUTPUT("Bluetooth connected");
      status.connected = 1;
    }
  } else {
    if (!bleMouse.isConnected()) {
      DEBUG_OUTPUT("Bluetooth disconnected");
      status.connected = 0;
      // bleMouse.end();
      // delay(100);
      // bleMouse = BleMouse("Hackaroony Mouse", "CorpSmash Industries");
      // bleMouse.begin();
    }
  }
  if(!in_jiggle) {
    set_led();
  }
}