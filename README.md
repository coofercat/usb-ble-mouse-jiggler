# USB-BLE Mouse Jiggler

## What is it?

This project uses an ESP32-S3 devkit board to convert a wired mouse into a BLE (bluetooth) mouse.
Additionally, it adds a 'mouse jiggler', which pretends the mouse is moving after a period of
inactivity. This will prevent the screen saver coming on, makes Teams think you're 'available'
and so on.

Unlike using a dedicated mouse jiggler, this option will only jiggle the mouse if you haven't moved
the mouse in a while. If you're doing some close control mouse work, this avoids 'random' pointer
movements you didn't actually intend. Also, coming via BLE means you don't need to actually plug
and unplug the jiggler, so you're unlikely to accidentally leave your jiggler in when you shouldn't.

In operation, there are two "timeouts". The first is an inactivity timeout (which comes set to 4m30s). If no
activity is detected in this time, then the mouse is jiggled.

The second timeout is the "jiggle time" which comes set to 30 minutes. After this time, no further
jiggling will take place, so your laptop will lock/sleep etc.

## How do I use it?

There are a number of steps required...

1. Obtain an ESP32-S3 DevkitC-1 board (eg. https://www.amazon.co.uk/dp/B0BVQLW2S6)
2. Obtain a wired mouse (5 mouse buttons and a vertical wheel are supported)
3. Solder bridge the OTG pad on the bottom of the ESP board
4. Optionally solder bridge the RGB pad on the top of the ESP board (if you want the
   onboard RGB LED to show status)
5. Install the EspUsbHost library (https://github.com/tanakamasayuki/EspUsbHost/releases)
   Note: At time of writing, the latest release did NOT include mouse support, so you'll need to
   use the master branch. Since this makes life difficult in Arduino UI, I've included the files
   here (there's no license in the project, so I'm assuming that's okay? We may need to keep an
   eye on updates though).
6. Install the ESP32-BLE-Mouse library (https://github.com/T-vK/ESP32-BLE-Mouse/releases)
7. Install the Locoduino RingBuffer library (via the Library Manager in the Arduino IDE).
8. If you're on a Mac, you'll need the magic serial chip driver to be able to program the ESP
   (https://www.wch-ic.com/downloads/CH341SER_MAC_ZIP.html). If you've already managed to program
   an ESP32-S3 via the 'COM' USB port, then you don't need to worry about this step.
9. Compile/upload the code in this repo to your device

Once all that's done, you ought to be able to just plug in a mouse and see movement being
reported in the Serial Monitor. In your laptop's Bluetooth settings, you should see either
"ESP Bluetooth", a mouse called "Bluetooth Device" or "Hackaroony Mouse" available, if you connect to it, then your mouse
should control your laptop.

## LED Colours

- Bright Red: Not connected to BLE
- Bright Blue: Connected, recent activity has been detected
- Bright Green: Connected, no activity detected so periodically jiggling
- Dim: Same as above, but outside the jiggle time, so not jiggling

## Limitations

- BLE to a Mac is (as per the project home page at time of writing) "buggy - some people report
  problems". I'm not quite sure what that means, but...
- Sometimes the USB side of things "disconnects" or is forgotten, so the mouse stops working.
  I'm not sure what's causing it, investigations continue.
- The underlying ESP BLE libraries seem to have a problem with the `notify()` function
  (see [issue](https://github.com/espressif/arduino-esp32/issues/8413)). To work around this,
  I've put mouse events into a RingBuffer and added a short delay after the calls to `notify()`.
  This seems to avoid the problem and makes the mouse feel more responsive and less "laggy".
- The BLE Mouse library has a PR against it waiting for a merge which fixes a problem where the
  library doesn't do advertising after a disconnect. You can use the PR repo to get this patch
  if this is a concern (see https://github.com/T-vK/ESP32-BLE-Mouse/pull/62).

## 3D Printer Files

There are a couple of files which might be useful if you plan on 3D printing a little box for your
ESP32-S3. The two halves of the box need some small screws to hold them together, which holds the
board in place quite snugly.
